{ pkgs, stdenv, opam2nix, fetchFromGitHub, makeRustPlatform }:

let
  src = fetchFromGitHub {
    owner = "mozilla";
    repo = "nixpkgs-mozilla";
    rev = "bf862af36145f5c1e566f93fe099e36bddeac0c8";
    sha256 = "0g6a0jss350k1ki7srhwkx62x93gxi2zxjdlii0m4bn5i7pxfcn4";
  };
in
with import "${src.out}/rust-overlay.nix" pkgs pkgs;

let
  rustPlatform = makeRustPlatform {
    cargo = latest.rustChannels.stable.rust;
    rustc = latest.rustChannels.stable.rust;
  };

  dalek_rs = rustPlatform.buildRustPackage rec {
    name = "dalek_rs-${version}";
    version = "0.1";
    src = ../.;
    cargoSha256 = "1x0q1yxhjrddyn6sj9v58v5x7f1xs1vr7clb6bfwm0ms5ph2bx32";
    meta = with stdenv.lib; {
      description = "A stub for ed25519 dalek.";
      homepage = https://gitlab.com/et4te/ocaml-dalek;
      license = licenses.mit;
    };
  };
in

with stdenv.lib;

stdenv.mkDerivation {
    name = "dalek";
    src = ../.;
    buildInputs = [dalek_rs] ++ opam2nix.build {
      specs = opam2nix.toSpecs [
	"ocamlbuild"
	"ocamlfind"
	"dune"
      ];
      ocamlAttr = "ocaml";
      ocamlVersion = "4.06.1";
    };
    buildPhase = ''
      mkdir -p target/release
      cp ${dalek_rs}/lib/libdalek_stubs.a target/release/libdalek_stubs.a
      cp ${dalek_rs}/lib/libdalek_stubs.so target/release/libdalek_stubs.so
      dune build
    '';
    installPhase = ''
      dune runtest
      cp -R _build $out/lib/dalek
    '';
}
