open Dalek

let _ =
  let message : string = "a message" in
  for _ = 0 to 1000 do
    let keypair = generate_keypair () in
    let signature = sign keypair message in
    assert (verify keypair message signature);
    let signature_bytes = chars_of_signature signature in
    assert (Array.length signature_bytes = 64);
    let signature_bytes' = chars_of_signature (signature_of_chars signature_bytes) in
    assert (signature_bytes = signature_bytes');
  done;

  let keypair = generate_keypair () in
  let public_bytes = chars_of_public keypair.public in
  assert (Array.length public_bytes = 32);
  let public_bytes' = chars_of_public (public_of_chars public_bytes) in
  assert (public_bytes = public_bytes');

  let secret_bytes = chars_of_secret keypair.secret in
  assert (Array.length secret_bytes = 32);
  let secret_bytes' = chars_of_secret (secret_of_chars secret_bytes) in
  assert (secret_bytes = secret_bytes');

