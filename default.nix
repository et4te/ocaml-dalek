{ pkgs ? import <nixpkgs> {} }:
pkgs.callPackage ./nix {
    opam2nix = pkgs.callPackage ./opam2nix-packages.nix {};
}
