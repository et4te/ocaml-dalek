type secret_key
type public_key
type expanded_secret_key

external secret_key_generate : unit -> secret_key = "secret_key_generate"
external expand_secret_key : secret_key -> expanded_secret_key = "expand_secret_key"
external public_key_from_secret_sha512 : secret_key -> public_key = "public_key_from_secret_sha512"

type signature

external sign_sha512 : expanded_secret_key -> string -> public_key -> signature = "sign_sha512"
external verify_sha512 : public_key -> string -> signature -> bool = "verify_sha512"

external chars_of_secret : secret_key -> char array = "chars_of_secret"
external secret_of_chars : char array -> secret_key = "secret_of_chars"

external chars_of_public : public_key -> char array = "chars_of_public"
external public_of_chars : char array -> public_key = "public_of_chars"

external chars_of_signature : signature -> char array = "chars_of_signature"
external signature_of_chars : char array -> signature = "signature_of_chars"
