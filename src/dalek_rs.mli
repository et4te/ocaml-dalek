
(* Public / Secret Key *)

type secret_key
type public_key
type expanded_secret_key

val secret_key_generate : unit -> secret_key
val expand_secret_key : secret_key -> expanded_secret_key
val public_key_from_secret_sha512 : secret_key -> public_key

(* Signatures *)

type signature

val sign_sha512 : expanded_secret_key -> string -> public_key -> signature
val verify_sha512 : public_key -> string -> signature -> bool

(* Byte conversion *)

val chars_of_secret : secret_key -> char array
val secret_of_chars : char array -> secret_key
val chars_of_public : public_key -> char array
val public_of_chars : char array -> public_key
val chars_of_signature : signature -> char array
val signature_of_chars : char array -> signature
