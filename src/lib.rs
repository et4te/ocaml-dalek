#[macro_use]
extern crate ocaml;
extern crate rand;
extern crate sha2;
extern crate ed25519_dalek;
extern crate serde;

use std::mem;

use rand::OsRng;
use sha2::Sha512;
use ed25519_dalek::{Signature, PublicKey, SecretKey, ExpandedSecretKey};
use ocaml::ToValue;

//------------------------------------------------------------------------------
// Ed25519
//------------------------------------------------------------------------------

extern "C" fn finalize_public_key(value: ocaml::core::Value) {
    let handle = ocaml::Value(value);
    let ptr = handle.custom_ptr_val_mut::<PublicKey>();
    mem::drop(ptr);
}

extern "C" fn finalize_secret_key(value: ocaml::core::Value) {
    let handle = ocaml::Value(value);
    let ptr = handle.custom_ptr_val_mut::<SecretKey>();
    mem::drop(ptr);
}

extern "C" fn finalize_expanded_secret_key(value: ocaml::core::Value) {
    let handle = ocaml::Value(value);
    let ptr = handle.custom_ptr_val_mut::<ExpandedSecretKey>();
    mem::drop(ptr);
}

extern "C" fn finalize_signature(value: ocaml::core::Value) {
    let handle = ocaml::Value(value);
    let ptr = handle.custom_ptr_val_mut::<Signature>();
    mem::drop(ptr);
}

caml!(secret_key_generate, |void|, <dest>, {
    let mut csprng: OsRng = OsRng::new().unwrap();
    let mut secret_key: SecretKey = SecretKey::generate(&mut csprng);
    let ptr = &mut secret_key as *mut SecretKey;
    mem::forget(secret_key);
    dest = ocaml::Value::alloc_custom(ptr, finalize_secret_key);
} -> dest);

caml!(chars_of_secret, |secret_key_handle|, <dest>, {
    let secret_key_ptr = secret_key_handle.custom_ptr_val::<SecretKey>();
    let mut bytes: Vec<u8> = (*secret_key_ptr).to_bytes().to_vec();
    dest = bytes.to_value();
} -> dest);

caml!(secret_of_chars, |bytes_handle|, <dest>, {
    let bytes: Vec<u8> = ocaml::FromValue::from_value(bytes_handle);
    let mut secret_key: SecretKey = SecretKey::from_bytes(&bytes).unwrap();
    let ptr = &mut secret_key as *mut SecretKey;
    mem::forget(secret_key);
    dest = ocaml::Value::alloc_custom(ptr, finalize_secret_key);
} -> dest);

caml!(expand_secret_key, |secret_key_handle|, <dest>, {
    let secret_key_ptr = secret_key_handle.custom_ptr_val::<SecretKey>();
    let mut expanded: ExpandedSecretKey = (*secret_key_ptr).expand::<Sha512>();
    let ptr = &mut expanded as *mut ExpandedSecretKey;
    mem::forget(expanded);
    dest = ocaml::Value::alloc_custom(ptr, finalize_expanded_secret_key);
} -> dest);

caml!(public_key_from_secret_sha512, |secret_key_handle|, <dest>, {
    let secret_key_ptr = secret_key_handle.custom_ptr_val::<SecretKey>();
    let mut public_key: PublicKey = PublicKey::from_secret::<Sha512>(&(*secret_key_ptr));
    let ptr = &mut public_key as *mut PublicKey;
    mem::forget(public_key);
    dest = ocaml::Value::alloc_custom(ptr, finalize_public_key);
} -> dest);

caml!(chars_of_public, |public_key_handle|, <dest>, {
    let public_key_ptr = public_key_handle.custom_ptr_val::<PublicKey>();
    let mut bytes: Vec<u8> = (*public_key_ptr).to_bytes().to_vec();
    dest = bytes.to_value();
} -> dest);

caml!(public_of_chars, |bytes_handle|, <dest>, {
    let bytes: Vec<u8> = ocaml::FromValue::from_value(bytes_handle);
    let mut public_key: PublicKey = PublicKey::from_bytes(&bytes).unwrap();
    let ptr = &mut public_key as *mut PublicKey;
    mem::forget(public_key);
    dest = ocaml::Value::alloc_custom(ptr, finalize_public_key);
} -> dest);

caml!(sign_sha512, |expanded_handle, message_handle, public_key_handle|, <dest>, {
    let expanded_ptr = expanded_handle.custom_ptr_val::<ExpandedSecretKey>();
    let public_key_ptr = public_key_handle.custom_ptr_val::<PublicKey>();
    let message: String = ocaml::FromValue::from_value(message_handle);
    let mut signature: Signature = (*expanded_ptr).sign::<Sha512>(message.as_bytes(), &(*public_key_ptr));
    let ptr = &mut signature as *mut Signature;
    mem::forget(signature);
    dest = ocaml::Value::alloc_custom(ptr, finalize_signature);
} -> dest);

caml!(verify_sha512, |public_key_handle, message_handle, signature_handle|, <dest>, {
    let public_key_ptr = public_key_handle.custom_ptr_val::<PublicKey>();
    let message: String = ocaml::FromValue::from_value(message_handle);
    let signature_ptr = signature_handle.custom_ptr_val::<Signature>();
    let result: bool = (*public_key_ptr).verify::<Sha512>(message.as_bytes(), &(*signature_ptr)).is_ok();
    dest = ocaml::Value::bool(result);
} -> dest);

caml!(chars_of_signature, |signature_handle|, <dest>, {
    let signature_ptr = signature_handle.custom_ptr_val::<Signature>();
    let mut bytes: Vec<u8> = (*signature_ptr).to_bytes().to_vec();
    dest = bytes.to_value();
} -> dest);

caml!(signature_of_chars, |bytes_handle|, <dest>, {
    let bytes: Vec<u8> = ocaml::FromValue::from_value(bytes_handle);
    let mut signature: Signature = Signature::from_bytes(&bytes).unwrap();
    let ptr = &mut signature as *mut Signature;
    mem::forget(signature);
    dest = ocaml::Value::alloc_custom(ptr, finalize_signature);
} -> dest);
