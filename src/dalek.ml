open Dalek_rs

(* Keypair *)

type keypair = {
  secret : secret_key;
  secret_expansion : expanded_secret_key;
  public : public_key;
}

let generate_keypair () =
  let secret_key = secret_key_generate () in
  { secret = secret_key;
    secret_expansion = expand_secret_key secret_key;
    public = public_key_from_secret_sha512 secret_key;
  }

(* Public / Secret Key *)

let generate_secret_key () =
  secret_key_generate ()

let public_key_from_secret secret_key =
  public_key_from_secret_sha512 secret_key

(* Signatures *)

let sign kp message =
  sign_sha512 kp.secret_expansion message kp.public

let verify kp message signature =
  verify_sha512 kp.public message signature

(* Char array conversion *)

let chars_of_secret secret =
  chars_of_secret secret

let secret_of_chars chars =
  secret_of_chars chars

let chars_of_public public =
  chars_of_public public

let public_of_chars chars =
  public_of_chars chars

let chars_of_signature signature =
  chars_of_signature signature

let signature_of_chars chars =
  signature_of_chars chars
