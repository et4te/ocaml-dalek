build:
	cargo build --release
	dune build

test: build
	dune runtest

clean:
	rm -f src/*.a src/*.so
	cargo clean
	dune clean
